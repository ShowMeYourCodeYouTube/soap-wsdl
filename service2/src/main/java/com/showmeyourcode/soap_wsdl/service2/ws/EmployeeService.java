package com.showmeyourcode.soap_wsdl.service2.ws;

import com.showmeyourcode.soap_wsdl.service2.exception.EmployeeAlreadyExistsException;
import com.showmeyourcode.soap_wsdl.service2.exception.EmployeeDoesNotExistException;
import com.showmeyourcode.soap_wsdl.service2.model.Employee;
import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebResult;
import jakarta.jws.WebService;
import jakarta.jws.soap.SOAPBinding;

@WebService(
        name = "EmployeeService",
        portName = "EmployeeServicePort",
        endpointInterface = "com.showmeyourcode.soap_wsdl.service2.ws.EmployeeService"
)
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use= SOAPBinding.Use.LITERAL)
public interface EmployeeService {

    @WebMethod(operationName = "GetEmployeeOperation")
    @WebResult(name="EmployeeResponse")
    Employee getEmployee(@WebParam(name = "id") int id);

    @WebMethod(operationName = "UpdateEmployeeOperation")
    @WebResult(name="EmployeeResponse")
    Employee updateEmployee(@WebParam(name = "id") int id, @WebParam(name = "name") String name) throws EmployeeDoesNotExistException;

    @WebMethod(operationName = "DeleteEmployeeOperation")
    @WebResult(name="IsOperationSuccessful")
    boolean deleteEmployee(@WebParam(name = "id") int id);

    @WebMethod(operationName = "AddEmployeeOperation")
    @WebResult(name="EmployeeResponse")
    Employee addEmployee(@WebParam(name = "id") int id, @WebParam(name = "name") String name) throws EmployeeAlreadyExistsException;
}
