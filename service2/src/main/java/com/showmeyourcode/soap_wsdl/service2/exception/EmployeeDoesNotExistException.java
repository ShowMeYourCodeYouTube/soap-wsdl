package com.showmeyourcode.soap_wsdl.service2.exception;

public class EmployeeDoesNotExistException extends Exception {
    public EmployeeDoesNotExistException() {
        super();
    }
}
