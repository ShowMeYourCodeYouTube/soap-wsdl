package com.showmeyourcode.soap_wsdl.service2.constant;

public class EndpointConstant {

    public static final String ACTUATOR_ENDPOINT = "/actuator";
    public static final String ACTUATOR_HEALTH_ENDPOINT = "/actuator/health";

    private EndpointConstant() {
    }
}
