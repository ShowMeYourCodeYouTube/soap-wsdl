package com.showmeyourcode.soap_wsdl.service2.config;

import com.showmeyourcode.soap_wsdl.service2.constant.WebServiceConstant;
import com.showmeyourcode.soap_wsdl.service2.ws.EmployeeService;
import jakarta.xml.ws.Endpoint;
import lombok.extern.slf4j.Slf4j;
import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class EndpointConfig {

    private final EmployeeService employeeService;

    public EndpointConfig(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @Bean(name = Bus.DEFAULT_BUS_ID)
    public SpringBus springBus() {
        return new SpringBus();
    }

    @Bean
    public Endpoint endpoint(SpringBus springBus) {
        log.info("Creating EmployeeService endpoint...");
        EndpointImpl endpoint = new EndpointImpl(springBus, employeeService);
        endpoint.publish(WebServiceConstant.WSDL_DEFINITION_NAME);
        return endpoint;
    }
}
