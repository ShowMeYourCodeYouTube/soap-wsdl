package com.showmeyourcode.soap_wsdl.service2.ws;

import com.showmeyourcode.soap_wsdl.service2.IntegrationTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;

import java.net.URI;
import java.net.URISyntaxException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class EmployeeServiceImplTests extends IntegrationTestBase {

    @Test
    void shouldGetEmployeeUsingPostRequest() throws URISyntaxException {
        String serviceUri = getEmployeeServiceUri();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Content-Type", "text/xml");
        httpHeaders.add("Accept", "application/xml");
        String body = """
                <soapenv:Envelope
                        xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                        xmlns:ws="http://ws.service2.soap_wsdl.showmeyourcode.com/">
                        <soapenv:Header/>
                            <soapenv:Body>
                              <ws:GetEmployeeOperation>
                              <id>2</id>
                            </ws:GetEmployeeOperation>
                        </soapenv:Body>
                </soapenv:Envelope>
                """;
        RequestEntity<String> requestEntity = new RequestEntity<>(body, httpHeaders, HttpMethod.POST, new URI(serviceUri));

        ResponseEntity<String> responseEntity = restTemplate.exchange(requestEntity, String.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals("<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><ns2:GetEmployeeOperationResponse xmlns:ns2=\"http://ws.service2.soap_wsdl.showmeyourcode.com/\"><EmployeeResponse><ns2:firstName>David</ns2:firstName><ns2:id>2</ns2:id></EmployeeResponse></ns2:GetEmployeeOperationResponse></soap:Body></soap:Envelope>", responseEntity.getBody());
    }

    @Test
    void shouldAddEmployeeUsingPostRequest() throws URISyntaxException {
        String serviceUri = getEmployeeServiceUri();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Content-Type", "text/xml");
        httpHeaders.add("Accept", "application/xml");
        String body = """
               <soapenv:Envelope
                    xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                    xmlns:ws="http://ws.service2.soap_wsdl.showmeyourcode.com/">
                    <soapenv:Header/>
                    <soapenv:Body>
                      <ws:AddEmployeeOperation>
                        <id>999</id>
                        <!--Optional:-->
                        <name>NewEmployee</name>
                      </ws:AddEmployeeOperation>
                   </soapenv:Body>
               </soapenv:Envelope>
               """;
        RequestEntity<String> requestEntity = new RequestEntity<>(body, httpHeaders, HttpMethod.POST, new URI(serviceUri));

        ResponseEntity<String> responseEntity = restTemplate.exchange(requestEntity, String.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals("<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><ns2:AddEmployeeOperationResponse xmlns:ns2=\"http://ws.service2.soap_wsdl.showmeyourcode.com/\"><EmployeeResponse><ns2:firstName>NewEmployee</ns2:firstName><ns2:id>999</ns2:id></EmployeeResponse></ns2:AddEmployeeOperationResponse></soap:Body></soap:Envelope>", responseEntity.getBody());
    }

    @Test
    void shouldDeleteEmployeeUsingPostRequest() throws URISyntaxException {
        String serviceUri = getEmployeeServiceUri();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Content-Type", "text/xml");
        httpHeaders.add("Accept", "application/xml");
        String body = """
               <soapenv:Envelope
                        xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" 
                        xmlns:ws="http://ws.service2.soap_wsdl.showmeyourcode.com/">
                    <soapenv:Header/>
                    <soapenv:Body>
                        <ws:DeleteEmployeeOperation>
                            <id>999</id>
                        </ws:DeleteEmployeeOperation>
                    </soapenv:Body>
               </soapenv:Envelope>
               """;
        RequestEntity<String> requestEntity = new RequestEntity<>(body, httpHeaders, HttpMethod.POST, new URI(serviceUri));

        ResponseEntity<String> responseEntity = restTemplate.exchange(requestEntity, String.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals("<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><ns2:DeleteEmployeeOperationResponse xmlns:ns2=\"http://ws.service2.soap_wsdl.showmeyourcode.com/\"><IsOperationSuccessful>true</IsOperationSuccessful></ns2:DeleteEmployeeOperationResponse></soap:Body></soap:Envelope>", responseEntity.getBody());
    }

    @Test
    void shouldUpdateEmployeeUsingPostRequest() throws URISyntaxException {
        String serviceUri = getEmployeeServiceUri();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Content-Type", "text/xml");
        httpHeaders.add("Accept", "application/xml");
        String body = """
                <soapenv:Envelope
                        xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                        xmlns:ws="http://ws.service2.soap_wsdl.showmeyourcode.com/">
                       <soapenv:Header/>
                       <soapenv:Body>
                            <ws:UpdateEmployeeOperation>
                            <id>1</id>
                            <!--Optional:-->
                            <name>Johnny2</name>
                       </ws:UpdateEmployeeOperation>
                   </soapenv:Body>
                </soapenv:Envelope>
                """;
        RequestEntity<String> requestEntity = new RequestEntity<>(body, httpHeaders, HttpMethod.POST, new URI(serviceUri));

        ResponseEntity<String> responseEntity = restTemplate.exchange(requestEntity, String.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals("<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><ns2:UpdateEmployeeOperationResponse xmlns:ns2=\"http://ws.service2.soap_wsdl.showmeyourcode.com/\"><EmployeeResponse><ns2:firstName>Johnny2</ns2:firstName><ns2:id>1</ns2:id></EmployeeResponse></ns2:UpdateEmployeeOperationResponse></soap:Body></soap:Envelope>", responseEntity.getBody());
    }
}
