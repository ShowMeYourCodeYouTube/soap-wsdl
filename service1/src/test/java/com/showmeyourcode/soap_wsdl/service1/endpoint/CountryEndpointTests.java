package com.showmeyourcode.soap_wsdl.service1.endpoint;

import com.showmeyourcode.soap_wsdl.service1.IntegrationTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;

import java.net.URI;
import java.net.URISyntaxException;

import static com.showmeyourcode.soap_wsdl.service1.constant.WebServiceConstant.LOCATION_URI;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class CountryEndpointTests extends IntegrationTestBase {

    @Test
    void shouldGetCountryForSpainUsingPostRequest() throws URISyntaxException {
        String wsdlUri = "http://localhost:" + serverPort + "/" + LOCATION_URI;
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Content-Type", "text/xml");
        httpHeaders.add("Accept", "application/xml");
        String body = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"showmeyourcode.com/soap-wsdl/service1\">\n" +
                "   <soapenv:Header/>\n" +
                "   <soapenv:Body>\n" +
                "      <ser:getCountryRequest>\n" +
                "         <ser:name>Spain</ser:name>\n" +
                "      </ser:getCountryRequest>\n" +
                "   </soapenv:Body>\n" +
                "</soapenv:Envelope>";
        RequestEntity<String> requestEntity = new RequestEntity<>(body, httpHeaders, HttpMethod.POST, new URI(wsdlUri));

        ResponseEntity<String> responseEntity = restTemplate.exchange(requestEntity, String.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals("<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"><SOAP-ENV:Header/><SOAP-ENV:Body><ns2:getCountryResponse xmlns:ns2=\"showmeyourcode.com/soap-wsdl/service1\"><ns2:country><ns2:name>Spain</ns2:name><ns2:population>46704314</ns2:population><ns2:capital>Madrid</ns2:capital><ns2:currency>EUR</ns2:currency></ns2:country></ns2:getCountryResponse></SOAP-ENV:Body></SOAP-ENV:Envelope>", responseEntity.getBody());
    }
}
