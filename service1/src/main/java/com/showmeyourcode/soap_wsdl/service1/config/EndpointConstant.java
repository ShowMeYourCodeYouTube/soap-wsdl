package com.showmeyourcode.soap_wsdl.service1.config;

public class EndpointConstant {

    public static final String ACTUATOR_ENDPOINT = "/actuator";
    public static final String ACTUATOR_HEALTH_ENDPOINT = "/actuator/health";

    private EndpointConstant() {
    }
}
