package com.showmeyourcode.soap_wsdl.main.config;

public class EndpointConstant {

    public static final String ACTUATOR_ENDPOINT = "/actuator";
    public static final String ACTUATOR_HEALTH_ENDPOINT = "/actuator/health";

    public static final String CONTROLLER_PREFIX = "/api/v1/services/";
    public static final String CONTROLLER_SERVICE1_PREFIX = "service1";
    public static final String CONTROLLER_SERVICE2_PREFIX = "service2";

    private EndpointConstant() {
    }
}
