package com.showmeyourcode.soap_wsdl.main.config;

import com.showmeyourcode.soap_wsdl.main.client.CountryClient;
import com.showmeyourcode.soap_wsdl.main.client.EmployeeClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Slf4j
@Configuration
public class ClientConfiguration {

    @Value("${service1.uri}")
    private String service1Uri;

    @Value("${service2.uri}")
    private String service2Uri;

    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("com.showmeyourcode.soap_wsdl.client.generated");
        return marshaller;
    }

    @Bean
    public CountryClient countryClient(Jaxb2Marshaller marshaller) {
        log.info("Configuring CountryClient for {}", service1Uri);
        CountryClient client = new CountryClient();
        client.setDefaultUri(service1Uri);
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }

    @Bean
    public EmployeeClient employeeClient(Jaxb2Marshaller marshaller) {
        log.info("Configuring EmployeeClient for {}", service2Uri);
        EmployeeClient client = new EmployeeClient();
        client.setDefaultUri(service2Uri);
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }
}
