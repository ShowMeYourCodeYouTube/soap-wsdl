package com.showmeyourcode.soap_wsdl.main.rest;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.http.HttpHeader;
import com.github.tomakehurst.wiremock.http.HttpHeaders;
import com.showmeyourcode.soap_wsdl.main.IntegrationTestBase;
import com.showmeyourcode.soap_wsdl.main.config.EndpointConstant;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;

import java.net.URI;
import java.net.URISyntaxException;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.configureFor;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class ServicesControllerTests extends IntegrationTestBase {

	@Test
	void shouldCallService1AndReturnSuccessfulResponse() throws URISyntaxException {
		HttpHeaders headersResponse = new HttpHeaders(
				new HttpHeader("SOAPAction",	""),
				new HttpHeader("Keep-Alive","timeout=60"),
				new HttpHeader("Accept","text/xml, text/html, image/gif, image/jpeg, *; q=.2, */*; q=.2"),
				new HttpHeader("Connection","keep-alive"),
				new HttpHeader("#status#","HTTP/1.1 200"),
				new HttpHeader("Content-Length","405"),
				new HttpHeader("Date","Mon, 12 Jun 2023 01:51:57 GMT"),
				new HttpHeader("Content-Type","text/xml;charset=utf-8")
		);

		configureFor(service1MockServer.getClient());
		stubFor(post(urlEqualTo("/ws"))
				.withHeader("Content-Type", WireMock.containing("text/xml; charset=UTF-8"))
				.willReturn(
						aResponse()
								.withStatus(200)
								.withHeaders(headersResponse)
								.withBodyFile("service1/success.xml")
				)
		);

		String endpointUri = EndpointConstant.CONTROLLER_PREFIX + EndpointConstant.CONTROLLER_SERVICE1_PREFIX;
		RequestEntity<String> requestEntity = new RequestEntity<>(HttpMethod.GET, new URI(endpointUri));
		ResponseEntity<String> responseEntity = restTemplate.exchange(requestEntity, String.class);

		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
		assertEquals(loadResourceAsString("__files/endpoint1/responseSuccess.json"),responseEntity.getBody());
	}

	@Test
	void shouldCallService2AndReturnSuccessfulResponse() throws URISyntaxException {
		HttpHeaders headersResponse = new HttpHeaders(
				new HttpHeader("Keep-Alive","timeout=60"),
				new HttpHeader("Connection","keep-alive"),
				new HttpHeader("#status#","HTTP/1.1 200"),
				new HttpHeader("Content-Length","277"),
				new HttpHeader("Date","Mon, 12 Jun 2023 01:51:57 GMT"),
				new HttpHeader("Content-Type","text/xml;charset=UTF-8")
		);

		configureFor(service2MockServer.getClient());
		stubFor(post(urlEqualTo("/services/service2-employee"))
				.withHeader("Content-Type", WireMock.containing("text/xml; charset=UTF-8"))
				.willReturn(
						aResponse()
								.withStatus(200)
								.withHeaders(headersResponse)
								.withBodyFile("service2/success.xml")
				)
		);

		String endpointUri = EndpointConstant.CONTROLLER_PREFIX + EndpointConstant.CONTROLLER_SERVICE2_PREFIX;
		RequestEntity<String> requestEntity = new RequestEntity<>(HttpMethod.GET, new URI(endpointUri));
		ResponseEntity<String> responseEntity = restTemplate.exchange(requestEntity, String.class);

		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
		assertEquals(loadResourceAsString("__files/endpoint2/responseSuccess.json"),responseEntity.getBody());
	}
}
